cursos = ["python", "django", "flask", "c", "c++", "c#", "java", "php"]

curso = cursos[5]
#se puede recorrer la lista usando indices negativos
sub = cursos[:7]
# [desde:hasta]
sub_salto = cursos[1:7:2]
#[desde:hasta:saltos]
sub_inverso = cursos[::-1]
#vizualizacion inversa de la lista
#Vizualizacion
#En este caso
"""
[:] Todos los elementos
[start:] Todos los elementos desde el indice establecido(start).
[:end] Todos los elementos desde el indice establecido hasta el indice establecido(end).
[start:end] Todos los elementos de un rango de indices.
[start:end:step] Todos los elementos de un rango de indices con saltos.
    |De igual forma, este listado es aplicable a las tuplas y los strings. 

"""
print(sub_inverso)